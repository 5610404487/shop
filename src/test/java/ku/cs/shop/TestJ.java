/**
 * Palida Sunthonkiti 5610404487
 */

package ku.cs.shop;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestJ {
	Inventory myInventory;
	Textbook textTest;
	Journal journalTest;
	Novel novelTest;
	Textbook textTestForAdd;
	@org.junit.Test
	@Before
	public void setup() {
		myInventory = new Inventory();
		textTest = new Textbook("Java Programming, 7th Edition","Joyce Farrell",
				"153","1285081951","7","English");
		journalTest = new Journal("Cornell Journal of Architecture 10: Spirits","Caroline O'Donnell",
				"24","0978506197","01092016","21");
		novelTest = new Novel("The Grave Robbers Chronicles","nan pai san shu",
				"12","7807407298","Shanghai Culture Press");
		myInventory.addBook(textTest);
		myInventory.addBook(journalTest);
		myInventory.addBook(novelTest);
	}
	@Test 
	public void testadd(){
		textTestForAdd = new Textbook("Software Engineering (9th Edition)","Ian Sommerville",
				"28","0137035152","9","English");
		myInventory.addBook(textTestForAdd);
		assertEquals(4,myInventory.getList().size());
	}
	@Test 
	public void testsearchisbn(){
		assertEquals("Cornell Journal of Architecture 10: Spirits",myInventory.searchISBN("0978506197"));
	}
	@Test 
	public void testsearchdetail(){
		assertEquals("Title: Cornell Journal of Architecture 10: Spirits\nAuthor: Caroline O'Donnell"
				+ "\nPrice: 24"
				+ "\nISBN: 0978506197"
				+ "\nPublish Date: 01092016"
				+ "\nVolume Number: 21\n"
				,myInventory.searchDetail("Caroline O'Donnell"));
	}
}
