/**
 * Palida Sunthonkiti 5610404487
 */


package ku.cs.shop;

import java.util.ArrayList;

public class Inventory {
	private ArrayList<Book> listBook = new ArrayList<Book>();
	
	public ArrayList getList() {
		return listBook;
	}
	public void addBook(Book objBook) {
		listBook.add(objBook);
	}
	public String searchISBN(String isbn) {
		if(listBook.size()!=0){
			for (Book bookinsearch : listBook){
				if(bookinsearch.getbookISBN().equals(isbn)){
					return ""+bookinsearch.getbookName();
				}	
			}
		}
		return "Not found";
	}
	public String searchDetail(String bookKeyword){
		String search = "";
		if(listBook.size()!=0){
			for (Book bookinsearch : listBook){
				if(bookinsearch.getbookName().equals(bookKeyword)){
					search+=bookinsearch.toString()+"\n";
				}	
				else if(bookinsearch.getbookAuthor().equals(bookKeyword)){
					search+=bookinsearch.toString()+"\n";
				}	
				else if(bookinsearch.getbookPrice().equals(bookKeyword)){
					search+=bookinsearch.toString()+"\n";
				}	
				else if(bookinsearch.getbookISBN().equals(bookKeyword)){
					search+=bookinsearch.toString()+"\n";
				}
				
				if(bookinsearch instanceof Journal){	
					if(((Journal) bookinsearch).getbookPublishDate().equals(bookKeyword)){
						search+=((Journal) bookinsearch).toString()+"\n";
					}
					if(((Journal) bookinsearch).getbookVolumeNumber().equals(bookKeyword)){
						search+=((Journal) bookinsearch).toString()+"\n";
					}
				}
				else if(bookinsearch instanceof Novel){	
					if(((Novel) bookinsearch).getbookPublisher().equals(bookKeyword)){
						search+=((Novel) bookinsearch).toString()+"\n";
					}
				}
				else if(bookinsearch instanceof Textbook){	
					if(((Textbook) bookinsearch).getbookEdition().equals(bookKeyword)){
						search+=((Textbook) bookinsearch).toString()+"\n";
					}
					if(((Textbook) bookinsearch).getbookLanguage().equals(bookKeyword)){
						search+=((Textbook) bookinsearch).toString()+"\n";
					}
				}	
			}
		}
		if(search.equals("")){
			return "Not found\n";
		}
		else{
		return search;
		}
	}
		public int searchISBNForAdd(String isbn) {
			int count=0;
			if(listBook.size()!=0){
				for (Book bookinsearch : listBook){
					count+=1;
					if(bookinsearch.getbookISBN().equals(isbn)){
						return count;
					}	
				}
			}
			return 0;
		}
	

}
