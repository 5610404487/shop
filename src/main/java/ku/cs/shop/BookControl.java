/**
 * Palida Sunthonkiti 5610404487
 */
package ku.cs.shop;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class BookControl {
	private ArrayList<Book> listBook = new ArrayList<Book>();
	private Inventory myInven;
	private Book objBook;
	private Journal objJournal;
	private Novel objNovel;
	private Textbook objTextbook;
	private int count = 0;
	private int status = 0;
	private BookShop shop;
	public BookControl(){
		myInven = new Inventory();
	}
	public String getAllBook(){
		String reportBook = "สินค้าที่มีอยู่\n\n";
		count =0;
		listBook = myInven.getList();
		for (Book i : listBook){
			count+=1;
			reportBook+=count+".\n"+i.toString()+"\n\n";
		}
		return reportBook+"Book in Inventory = "+count;
	}
	
	public void addNormal(String Name, String Author, String Price, String ISBN) {
		objBook = new Book(Name,Author,Price,ISBN);
		myInven.addBook(objBook);
		writeFile("\"Book\"");
		writeFile(Name);
		writeFile(Author);
		writeFile(Price);
		writeFile(ISBN);
	}
	public void addJornal(String NameJournal, String AuthorJournal, String PriceJournal,
			String ISBNJournal, String Date, String Number) {
		objJournal = new Journal(NameJournal,AuthorJournal,PriceJournal,ISBNJournal,Date,Number);
		writeFile("\"Journal\"");
		myInven.addBook(objJournal);
		writeFile(NameJournal);
		writeFile(AuthorJournal);
		writeFile(PriceJournal);
		writeFile(ISBNJournal);
		writeFile(Date);
		writeFile(Number);
		
	}
	public void addNovel(String NameNovel, String AuthorNovel, String PriceNovel, String ISBNNovel,String Publisher) {
		objNovel = new Novel(NameNovel,AuthorNovel,PriceNovel,ISBNNovel,Publisher);
		myInven.addBook(objNovel);
		writeFile("\"Novel\"");
		writeFile(NameNovel);
		writeFile(AuthorNovel);
		writeFile(PriceNovel);
		writeFile(ISBNNovel);
		writeFile(Publisher);
	}
	public void addTextBook(String NameTxtbook, String AuthorTxtbook, String PriceTxtbook,
			String ISBNTxtbook, String Edition, String Lang) {
		objTextbook = new Textbook(NameTxtbook,AuthorTxtbook,PriceTxtbook,ISBNTxtbook,Edition,Lang);
		myInven.addBook(objTextbook);
		writeFile("\"Textbook\"");
		writeFile(NameTxtbook);
		writeFile(NameTxtbook);
		writeFile(AuthorTxtbook);
		writeFile(PriceTxtbook);
		writeFile(ISBNTxtbook);
		writeFile(Edition);
		writeFile(Lang);
		
	}
	public String searchISBN(String isbn) {
		return myInven.searchISBN(isbn);
	}
	public String searchDetail(String detail) {
		return myInven.searchDetail(detail);
	}
	
//	public String addBookToBasket(String ISBN){
//		String getName = shop.addtoBasket(ISBN);
//		return getName;	
//	}
	
	public void readFile(){
		String a1 = "" ;
		String a2 = "";
		String a3= "" ;
		String a4= "" ;
		String a5 = "";
		String a6= "" ;
		String a7= "" ;
		String path = "data.txt";
		File file = new File(path);
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
//				System.out.println(line);
				if(line.equals("\"Book\"")||status ==1){
					status = 1;
					
					if (count==1){
						a1 = line;
					}
					else if (count==2){
						a2 = line;
					}
					else if (count==3){
						a3 = line;
					}
					else if (count==4){
						a4=line;
						objBook = new Book(a1,a2,a3,a4);
						myInven.addBook(objBook);
						a1 ="";
						a2 ="";
						a3 ="";
						a4 ="";
						status=0;
						count=0;
					}
					count+=1;
					if(status ==0){
						count=0;
					}
				}
				
				else if(line.equals("\"Journal\"")||status ==2){
					status = 2;
				
					if (count==1){
						a1 = line;
					}
					else if (count==2){
						a2 = line;
					}
					else if (count==3){
						a3 = line;
					}
					else if (count==4){
						a4 = line;
					}
					else if (count==5){
						a5 = line;
					}
					else if (count==6){
						a6=line;
						objJournal = new Journal(a1,a2,a3,a4,a5,a6);
						myInven.addBook(objJournal);
						a1 ="";
						a2 ="";
						a3 ="";
						a4 ="";
						a5 ="";
						a6 ="";
						status=0;
						count=0;
					}
					count+=1;
					if(status ==0){
						count=0;
					}
				}
				else if(line.equals("\"Novel\"")||status ==3){
					status = 3;
					
					if (count==1){
						a1 = line;
					}
					else if (count==2){
						a2 = line;
					}
					else if (count==3){
						a3 = line;
					}
					else if (count==4){
						a4 = line;
					}
					else if (count==5){
						a5=line;
						objNovel = new Novel(a1,a2,a3,a4,a5);
						myInven.addBook(objNovel);
						a1 ="";
						a2 ="";
						a3 ="";
						a4 ="";
						a5="";
						status=0;
						count=0;
					}
					count+=1;
					if(status ==0){
						count=0;
					}
				}
				else if(line.equals("\"Textbook\"")||status ==4){
					status = 4;
			
					if (count==1){
						a1 = line;
					}
					else if (count==2){
						a2 = line;
					}
					else if (count==3){
						a3 = line;
					}
					else if (count==4){
						a4 = line;
					}
					else if (count==5){
						a5 = line;
					}
					else if (count==6){
						a6=line;
						objTextbook = new Textbook(a1,a2,a3,a4,a5,a6);
						myInven.addBook(objTextbook);
						a1 ="";
						a2 ="";
						a3 ="";
						a4 ="";
						a5 ="";
						a6 ="";
						status=0;
						count=0;
					}
					count+=1;
					if(status ==0){
						count=0;
					}
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeFile(String getStringWrite){
		try
		{
		    String filename= "data.txt";
		    FileWriter fw = new FileWriter(filename,true);
		    fw.write(getStringWrite+"\n");
		    fw.close();
		}
		catch(IOException ioe)
		{
		    System.err.println("IOException: " + ioe.getMessage());
		}
	}
	
}
