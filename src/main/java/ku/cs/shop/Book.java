/**
 * Palida Sunthonkiti 5610404487
 */


package ku.cs.shop;

public class Book {
	private String bookName; 
	private String bookAuthor; 
	private String bookPrice; 
	private String bookISBN; 
	
	public Book(String bookName,String bookAuthor,String bookPrice,String bookISBN){
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.bookPrice = bookPrice;
		this.bookISBN = bookISBN;
	}
	public String toString(){
		return "Title: "+ bookName+
				"\nAuthor: "+bookAuthor+
				"\nPrice: "+bookPrice+
				"\nISBN: "+bookISBN
				;
	}
	public String getbookName(){
		return bookName;
	}
	public String getbookAuthor(){
		return bookAuthor;
	}
	public String getbookPrice(){
		return bookPrice;
	}
	public String getbookISBN(){
		return bookISBN;
	}
	
	
	
	

}
