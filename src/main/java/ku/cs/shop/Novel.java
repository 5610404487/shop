/**
 * Palida Sunthonkiti 5610404487
 */

package ku.cs.shop;

public class Novel extends Book{

	private String bookIPublisher;


	public Novel(String bookName, String bookAuthor, String bookPrice,
			String bookISBN,String bookPublisher) {
		super(bookName, bookAuthor, bookPrice, bookISBN);
		// TODO Auto-generated constructor stub
		this.bookIPublisher = bookPublisher;
	}
	public String getbookPublisher(){
		return bookIPublisher;
	}
	public String toString(){
		return super.toString()+
				"\nPublisher: "+bookIPublisher
				;
	}
}
