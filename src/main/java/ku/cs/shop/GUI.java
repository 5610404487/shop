/**
 * Palida Sunthonkiti 5610404487
 */

package ku.cs.shop;


import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JList;

public class GUI {

	private JFrame frame;
	private JTextArea txtDetail;
	private JLabel lblClothesShop;
	private JLabel label;
	private JPanel panel;
	private JPanel panelShowDetail;
	private JButton buttonSearchISBN;
	private JButton buttonSearchDetail;
	private JTabbedPane tabAll;
	private JLayeredPane tabBook;
	private JLayeredPane tabJournal;
	private JLayeredPane tabNovel;
	private JLayeredPane tabTextBook;
	private JPanel panelTabJournal;
	private JPanel panelTabNovel;
	private JPanel panelTabTectBook;
	private JLabel lblName;
	private JLabel lblAuthor;
	private JLabel lblPrice;
	private JLabel lblIsbn;
	private JTextField txtName;
	private JTextField txtAuthor;
	private JTextField txtPrice;
	private JTextField txtISBN;
	private JButton btnClear;
	private JButton btnAdd;
	private JTextField txtSearchISBN;
	private JTextField txtSearchDetail;
	private JTextField txtNameJournal;
	private JTextField txtAuthorJournal;
	private JTextField txtPriceJournal;
	private JTextField txtISBNJournal;
	private JTextField txtDate;
	private JLabel lblVolumeNumber;
	private JTextField txtNumber;
	private JLabel lblNameNovel;
	private JLabel lblAuthotNovel;
	private JLabel lblPriceNovel;
	private JLabel lblISBNNovel;
	private JLabel lblPublicsher;
	private JTextField txtNameNovel;
	private JTextField txtAuthorNovel;
	private JTextField txtPriceNovel;
	private JTextField txtISBNNovel;
	private JTextField txtPublisher;
	private JButton btnAddNovel;
	private JButton brnClear;
	private JLabel lblNameTxtbook;
	private JLabel lblAuthorTxtbook;
	private JLabel lblPriceTxtbook;
	private JLabel lblISBNTxtbook;
	private JLabel lblEdition;
	private JLabel lblLanguage;
	private JTextField txtNameTxtbook;
	private JTextField txtAuthorTxtbook;
	private JTextField txtPriceTxtbook;
	private JTextField txtISBNTxtbook;
	private JTextField txtEdition;
	private JTextField txtLang;
	private JButton btnAddTxt;
	private JButton btnClearTxt;
	private BookControl control;
	private JButton btnNewButton;
	private JButton showAllBook;
	private String getNameFromISBN = "";
	private JList listBasket;
	private DefaultListModel listModel;
	private JScrollPane scrollPane_1;
	
	public void setGUI(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUI() {
		control = new BookControl();
		control.readFile();
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 153));
		frame.setBounds(100, 100, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panelAdd = new JPanel();
		panelAdd.setBorder(new TitledBorder(new LineBorder(new Color(100, 100 ,100), 1), "Add Book") );
		panelAdd.setBackground(new Color(153, 204, 153));
		panelAdd.setBounds(10, 11, 309, 234);
		frame.getContentPane().add(panelAdd);
		panelAdd.setLayout(null);
		
		tabAll = new JTabbedPane(JTabbedPane.TOP);
		tabAll.setBounds(10, 22, 289, 201);
		panelAdd.add(tabAll);
		
		tabBook = new JLayeredPane();
		tabBook.setBackground(SystemColor.info);
		tabBook.setForeground(SystemColor.info);
		tabAll.addTab("Normal", null, tabBook, null);
		tabBook.setLayout(null);
		
		JPanel panelTabBook = new JPanel();
		panelTabBook.setBackground(new Color(238, 232, 170));
		panelTabBook.setBounds(0, 0, 284, 173);
		tabBook.add(panelTabBook);
		panelTabBook.setLayout(null);
		
		//Normal
		lblName = new JLabel("Name :");
		lblName.setBounds(10, 14, 58, 14);
		panelTabBook.add(lblName);
		
		lblAuthor = new JLabel("Author :");
		lblAuthor.setBounds(10, 51, 79, 14);
		panelTabBook.add(lblAuthor);
		
		lblPrice = new JLabel("Price :");
		lblPrice.setBounds(10, 86, 39, 14);
		panelTabBook.add(lblPrice);
		
		lblIsbn = new JLabel("ISBN :");
		lblIsbn.setBounds(10, 129, 46, 14);
		panelTabBook.add(lblIsbn);
		
		txtName = new JTextField();
		txtName.setBounds(57, 11, 217, 29);
		panelTabBook.add(txtName);
		txtName.setColumns(10);
		
		txtAuthor = new JTextField();
		txtAuthor.setBounds(57, 48, 217, 29);
		panelTabBook.add(txtAuthor);
		txtAuthor.setColumns(10);
		
		txtPrice = new JTextField();
		txtPrice.setBounds(56, 86, 131, 29);
		panelTabBook.add(txtPrice);
		txtPrice.setColumns(10);
		
		txtISBN = new JTextField();
		txtISBN.setColumns(10);
		txtISBN.setBounds(56, 122, 131, 29);
		panelTabBook.add(txtISBN);
		
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			if(txtName.getText().equals("")||txtAuthor.getText().equals("")||txtPrice.getText().equals("")||txtISBN.getText().equals("")){
				JFrame parent = new JFrame();
			    JOptionPane.showMessageDialog(parent, "Please fill in all blanks.");
			}
			else{
				String getBook = "\nAdd Book Succeed\nTitle: "+txtName.getText()+"\nAuthor: "+txtAuthor.getText()+"\nPrice: "
			+txtPrice.getText()+"\nISBN: "+txtISBN.getText()+"\n---------------------------";
				txtDetail.setText(getBook);
				control.addNormal(txtName.getText(),txtAuthor.getText(),txtPrice.getText(),txtISBN.getText());
				txtName.setText("");
				txtAuthor.setText("");
				txtPrice.setText("");
				txtISBN.setText("");
			}
				
				
			}
		});
		btnAdd.setBounds(195, 86, 79, 53);
		panelTabBook.add(btnAdd);
		
		btnClear = new JButton("clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtName.setText("");
				txtAuthor.setText("");
				txtPrice.setText("");
				txtISBN.setText("");
			}
		});
		btnClear.setBounds(195, 150, 79, 23);
		panelTabBook.add(btnClear);
		//EndNormal
		//Journal
		tabJournal = new JLayeredPane();
		tabAll.addTab("Journal", null, tabJournal, null);
		
		panelTabJournal = new JPanel();
		panelTabJournal.setBackground(new Color(255, 228, 181));
		panelTabJournal.setBounds(0, 0, 284, 173);
		tabJournal.add(panelTabJournal);
		panelTabJournal.setLayout(null);
		
		JLabel lblNameJournal = new JLabel("Name :");
		lblNameJournal.setBounds(10, 15, 58, 14);
		panelTabJournal.add(lblNameJournal);
		
		JLabel lblAuthorJournal = new JLabel("Author :");
		lblAuthorJournal.setBounds(10, 43, 79, 14);
		panelTabJournal.add(lblAuthorJournal);
		
		JLabel lblPriceJournal = new JLabel("Price :");
		lblPriceJournal.setBounds(10, 74, 39, 14);
		panelTabJournal.add(lblPriceJournal);
		
		JLabel lblISBNJournal = new JLabel("ISBN :");
		lblISBNJournal.setBounds(10, 99, 46, 14);
		panelTabJournal.add(lblISBNJournal);
		
		JLabel lblPublicDate = new JLabel("Publish Date :");
		lblPublicDate.setBounds(10, 127, 89, 14);
		panelTabJournal.add(lblPublicDate);
		
		lblVolumeNumber = new JLabel("Volume Number :");
		lblVolumeNumber.setBounds(10, 152, 138, 14);
		panelTabJournal.add(lblVolumeNumber);
		
		txtNameJournal = new JTextField();
		txtNameJournal.setColumns(10);
		txtNameJournal.setBounds(57, 11, 217, 23);
		panelTabJournal.add(txtNameJournal);
		
		txtAuthorJournal = new JTextField();
		txtAuthorJournal.setColumns(10);
		txtAuthorJournal.setBounds(57, 39, 217, 23);
		panelTabJournal.add(txtAuthorJournal);
		
		txtPriceJournal = new JTextField();
		txtPriceJournal.setColumns(10);
		txtPriceJournal.setBounds(56, 68, 131, 23);
		panelTabJournal.add(txtPriceJournal);
		
		txtISBNJournal = new JTextField();
		txtISBNJournal.setColumns(10);
		txtISBNJournal.setBounds(56, 95, 131, 23);
		panelTabJournal.add(txtISBNJournal);
		
		txtDate = new JTextField();
		txtDate.setBounds(89, 124, 98, 20);
		panelTabJournal.add(txtDate);
		txtDate.setColumns(10);
		
		txtNumber = new JTextField();
		txtNumber.setColumns(10);
		txtNumber.setBounds(120, 149, 67, 20);
		panelTabJournal.add(txtNumber);
		
		JButton btnAddJournal = new JButton("Add");
		btnAddJournal.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
					if(txtNameJournal.getText().equals("")||txtAuthorJournal.getText().equals("")||
							txtPriceJournal.getText().equals("")||txtISBNJournal.getText().equals("")||txtDate.getText().equals("")||txtNumber.getText().equals("")){
						JFrame parent = new JFrame();
					    JOptionPane.showMessageDialog(parent, "Please fill in all blanks.");
					}
					else{
						String getBook = "\nAdd Journal Succeed\nTitle: "+txtNameJournal.getText()+"\nAuthor: "+txtAuthorJournal.getText()+"\nPrice: "
					+txtPriceJournal.getText()+"\nISBN: "+txtISBNJournal.getText()+"\nPublish Date: "
								+txtDate.getText()+"\nVolume Number: "+txtNumber.getText()+"\n---------------------------";
						txtDetail.setText(getBook);
						control.addJornal(txtNameJournal.getText(),txtAuthorJournal.getText(),
								txtPriceJournal.getText(),txtISBNJournal.getText(),txtDate.getText(),txtNumber.getText());
						txtNameJournal.setText("");
						txtAuthorJournal.setText("");
						txtPriceJournal.setText("");
						txtISBNJournal.setText("");
						txtDate.setText("");
						txtNumber.setText("");
					}
					
			}
		});
		btnAddJournal.setBounds(195, 86, 79, 53);
		panelTabJournal.add(btnAddJournal);
		
		JButton btnClearJournal = new JButton("clear");
		btnClearJournal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNameJournal.setText("");
				txtAuthorJournal.setText("");
				txtPriceJournal.setText("");
				txtISBNJournal.setText("");
				txtDate.setText("");
				txtNumber.setText("");
			}
		});
		btnClearJournal.setBounds(195, 150, 79, 23);
		panelTabJournal.add(btnClearJournal);
		//EndJournal
		//Novel
		tabNovel = new JLayeredPane();
		tabAll.addTab("Novel", null, tabNovel, null);
		tabNovel.setLayout(null);
		
		panelTabNovel = new JPanel();
		panelTabNovel.setBackground(new Color(238, 232, 170));
		panelTabNovel.setBounds(0, 0, 284, 173);
		tabNovel.add(panelTabNovel);
		panelTabNovel.setLayout(null);
		
		lblNameNovel = new JLabel("Name :");
		lblNameNovel.setBounds(10, 15, 58, 14);
		panelTabNovel.add(lblNameNovel);
		
		lblAuthotNovel = new JLabel("Author :");
		lblAuthotNovel.setBounds(10, 43, 79, 14);
		panelTabNovel.add(lblAuthotNovel);
		
		lblPriceNovel = new JLabel("Price :");
		lblPriceNovel.setBounds(10, 74, 39, 14);
		panelTabNovel.add(lblPriceNovel);
		
		lblISBNNovel = new JLabel("ISBN :");
		lblISBNNovel.setBounds(10, 99, 46, 14);
		panelTabNovel.add(lblISBNNovel);
		
		lblPublicsher = new JLabel("Publisher :");
		lblPublicsher.setBounds(10, 129, 89, 14);
		panelTabNovel.add(lblPublicsher);
		
		txtNameNovel = new JTextField();
		txtNameNovel.setColumns(10);
		txtNameNovel.setBounds(57, 11, 217, 23);
		panelTabNovel.add(txtNameNovel);
		
		txtAuthorNovel = new JTextField();
		txtAuthorNovel.setColumns(10);
		txtAuthorNovel.setBounds(57, 39, 217, 23);
		panelTabNovel.add(txtAuthorNovel);
		
		txtPriceNovel = new JTextField();
		txtPriceNovel.setColumns(10);
		txtPriceNovel.setBounds(56, 68, 131, 23);
		panelTabNovel.add(txtPriceNovel);
		
		txtISBNNovel = new JTextField();
		txtISBNNovel.setColumns(10);
		txtISBNNovel.setBounds(56, 95, 131, 23);
		panelTabNovel.add(txtISBNNovel);
		
		txtPublisher = new JTextField();
		txtPublisher.setColumns(10);
		txtPublisher.setBounds(76, 124, 111, 23);
		panelTabNovel.add(txtPublisher);
		
		btnAddNovel = new JButton("Add");
		btnAddNovel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtNameNovel.getText().equals("")||txtAuthorNovel.getText().equals("")||txtPriceNovel.getText().equals("")
						||txtISBNNovel.getText().equals("")||txtPublisher.getText().equals("")){
					JFrame parent = new JFrame();
				    JOptionPane.showMessageDialog(parent, "Please fill in all blanks.");
				}
				else{
					String getBook = "\nAdd Novel Succeed\nTitle: "+txtNameNovel.getText()+
							"\nAuthorNovel: "+txtAuthor.getText()+"\nPrice: "
				+txtPriceNovel.getText()+"\nISBN: "+txtISBNNovel.getText()+"\nPublisher: "
							+txtPublisher.getText()+"\n---------------------------";
					txtDetail.setText(getBook);
					control.addNovel(txtNameNovel.getText(),txtAuthorNovel.getText(),
							txtPriceNovel.getText(),txtISBNNovel.getText(),txtPublisher.getText());
					txtNameNovel.setText("");
					txtAuthorNovel.setText("");
					txtPriceNovel.setText("");
					txtISBNNovel.setText("");
					txtPublisher.setText("");
				}
			}
		});
		btnAddNovel.setBounds(195, 86, 79, 53);
		panelTabNovel.add(btnAddNovel);
		
		brnClear = new JButton("clear");
		brnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNameNovel.setText("");
				txtAuthorNovel.setText("");
				txtPriceNovel.setText("");
				txtISBNNovel.setText("");
				txtPublisher.setText("");
			}
		});
		brnClear.setBounds(195, 150, 79, 23);
		panelTabNovel.add(brnClear);
		//EndNovel
		//TextBook
		tabTextBook = new JLayeredPane();
		tabAll.addTab("Text Book", null, tabTextBook, null);
		tabTextBook.setLayout(null);
		
		panelTabTectBook = new JPanel();
		panelTabTectBook.setBackground(new Color(245, 222, 179));
		panelTabTectBook.setBounds(0, 0, 284, 173);
		tabTextBook.add(panelTabTectBook);
		panelTabTectBook.setLayout(null);
		
		lblNameTxtbook = new JLabel("Name :");
		lblNameTxtbook.setBounds(10, 15, 58, 14);
		panelTabTectBook.add(lblNameTxtbook);
		
		lblAuthorTxtbook = new JLabel("Author :");
		lblAuthorTxtbook.setBounds(10, 43, 79, 14);
		panelTabTectBook.add(lblAuthorTxtbook);
		
		lblPriceTxtbook = new JLabel("Price :");
		lblPriceTxtbook.setBounds(10, 74, 39, 14);
		panelTabTectBook.add(lblPriceTxtbook);
		
		lblISBNTxtbook = new JLabel("ISBN :");
		lblISBNTxtbook.setBounds(10, 99, 46, 14);
		panelTabTectBook.add(lblISBNTxtbook);
		
		lblEdition = new JLabel("Edition : ");
		lblEdition.setBounds(10, 127, 89, 14);
		panelTabTectBook.add(lblEdition);
		
		lblLanguage = new JLabel("Language :");
		lblLanguage.setBounds(10, 152, 89, 14);
		panelTabTectBook.add(lblLanguage);
		
		txtNameTxtbook = new JTextField();
		txtNameTxtbook.setColumns(10);
		txtNameTxtbook.setBounds(57, 11, 217, 23);
		panelTabTectBook.add(txtNameTxtbook);
		
		txtAuthorTxtbook = new JTextField();
		txtAuthorTxtbook.setColumns(10);
		txtAuthorTxtbook.setBounds(57, 39, 217, 23);
		panelTabTectBook.add(txtAuthorTxtbook);
		
		txtPriceTxtbook = new JTextField();
		txtPriceTxtbook.setColumns(10);
		txtPriceTxtbook.setBounds(56, 68, 131, 23);
		panelTabTectBook.add(txtPriceTxtbook);
		
		txtISBNTxtbook = new JTextField();
		txtISBNTxtbook.setColumns(10);
		txtISBNTxtbook.setBounds(56, 95, 131, 23);
		panelTabTectBook.add(txtISBNTxtbook);
		
		txtEdition = new JTextField();
		txtEdition.setColumns(10);
		txtEdition.setBounds(57, 124, 58, 20);
		panelTabTectBook.add(txtEdition);
		
		txtLang = new JTextField();
		txtLang.setColumns(10);
		txtLang.setBounds(89, 149, 98, 20);
		panelTabTectBook.add(txtLang);
		
		btnAddTxt = new JButton("Add");
		btnAddTxt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtNameTxtbook.getText().equals("")||txtAuthorTxtbook.getText().equals("")||
						txtPriceTxtbook.getText().equals("")||txtISBNTxtbook.getText().equals("")||
						txtEdition.getText().equals("")||txtLang.getText().equals("")){
					JFrame parent = new JFrame();
				    JOptionPane.showMessageDialog(parent, "Please fill in all blanks.");
				}
				else{
					String getBook = "\nAdd Text Book Succeed\nTitle: "+txtNameTxtbook.getText()+"\nAuthor: "
				+txtAuthorTxtbook.getText()+"\nPrice: "
				+txtPriceTxtbook.getText()+"\nISBN: "+txtISBNTxtbook.getText()+
				"\nEdition: "+txtEdition.getText()+"\nLanguage: "+txtLang.getText()+"\n---------------------------";
					txtDetail.setText(getBook);
					control.addTextBook(txtNameTxtbook.getText(),txtAuthorTxtbook.getText(),
							txtPriceTxtbook.getText(),txtISBNTxtbook.getText(),txtEdition.getText(),txtLang.getText());
					txtNameTxtbook.setText("");
					txtAuthorTxtbook.setText("");
					txtPriceTxtbook.setText("");
					txtISBNTxtbook.setText("");
					txtEdition.setText("");
					txtLang.setText("");
				}
			}
		});
		btnAddTxt.setBounds(195, 86, 79, 53);
		panelTabTectBook.add(btnAddTxt);
		
		btnClearTxt = new JButton("clear");
		btnClearTxt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNameTxtbook.setText("");
				txtAuthorTxtbook.setText("");
				txtPriceTxtbook.setText("");
				txtISBNTxtbook.setText("");
				txtEdition.setText("");
				txtLang.setText("");
				
			}
		});
		btnClearTxt.setBounds(195, 150, 79, 23);
		panelTabTectBook.add(btnClearTxt);
		//EndTextBook
		
		
		JPanel panelSearch = new JPanel();
		panelSearch.setBackground(new Color(51, 153, 153));
		panelSearch.setBorder(new TitledBorder(new LineBorder(new Color(100, 100 ,100), 1), "Search") );
		panelSearch.setBounds(10, 256, 309, 195);
		frame.getContentPane().add(panelSearch);
		panelSearch.setLayout(null);
		//searchISBN
		buttonSearchISBN = new JButton("ค้นหาจากISBN");
		buttonSearchISBN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String datasearchISBN = control.searchISBN(txtSearchISBN.getText());
				txtDetail.setText("\nSearch by ISBN : \n"+datasearchISBN+"\n---------------------------");
				getNameFromISBN=datasearchISBN;
			}
		});
		buttonSearchISBN.setBounds(176, 45, 123, 39);
		panelSearch.add(buttonSearchISBN);
		
		buttonSearchDetail = new JButton("ค้นหา");
		buttonSearchDetail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String datasearchDetail = control.searchDetail(txtSearchDetail.getText());
				txtDetail.setText("\nSearch Detail : \n"+datasearchDetail+"---------------------------");
				
			}
		});
		//searchDetail
		buttonSearchDetail.setBounds(174, 113, 125, 39);
		panelSearch.add(buttonSearchDetail);
		
		txtSearchISBN = new JTextField();
		txtSearchISBN.setBounds(23, 45, 143, 39);
		panelSearch.add(txtSearchISBN);
		txtSearchISBN.setColumns(10);
		
		txtSearchDetail = new JTextField();
		
		txtSearchDetail.setBounds(23, 113, 143, 39);
		panelSearch.add(txtSearchDetail);
		txtSearchDetail.setColumns(10);
		
		showAllBook = new JButton("ดูสินค้าทั้งหมด");
		showAllBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String allBook = control.getAllBook();
				txtDetail.setText(allBook);
			}
		});
		showAllBook.setBounds(23, 163, 143, 23);
		panelSearch.add(showAllBook);
		
//		btnNewButton = new JButton("New button");
//		btnNewButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				ArrayList<Book> listtest ;
//				listtest = myInven.getList();
//				for (Book bookinsearch : listtest ){
//					System.out.println(bookinsearch.toString());
//				}
//			}
//		});
//		btnNewButton.setBounds(81, 163, 89, 23);
//		panelSearch.add(btnNewButton);
		
		panelShowDetail = new JPanel();
		panelShowDetail.setBackground(new Color(255, 255, 204));
		panelShowDetail.setBounds(322, 153, 284, 298);
		frame.getContentPane().add(panelShowDetail);
		panelShowDetail.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 264, 276);
		panelShowDetail.add(scrollPane);
		
		txtDetail = new JTextArea();
		scrollPane.setViewportView(txtDetail);
		txtDetail.setText("=============Book store=============");
		
		panel = new JPanel();
		panel.setBackground(new Color(255, 255, 153));
		panel.setBounds(329, 22, 445, 120);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblClothesShop = new JLabel("Book store");
		lblClothesShop.setFont(new Font("Tahoma", Font.PLAIN, 38));
		lblClothesShop.setBounds(93, 0, 310, 82);
		panel.add(lblClothesShop);
		
		label = new JLabel("ร้านขายหนังสือออนไลน์");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(191, 59, 212, 50);
		panel.add(label);
		
		JPanel panelBuy = new JPanel();
		panelBuy.setBackground(new Color(204, 204, 153));
		panelBuy.setBorder(new TitledBorder(new LineBorder(new Color(100, 100 ,100), 1), "Buy Book") );
		panelBuy.setBounds(616, 153, 158, 298);
		frame.getContentPane().add(panelBuy);
		panelBuy.setLayout(null);
		
		JButton addToBasket = new JButton("เลือกใส่ตะกร้า");
		addToBasket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(getNameFromISBN.equals("")){
					JFrame parent = new JFrame();
				    JOptionPane.showMessageDialog(parent, "Pleach Search ISBN for Add");
				}
				else{
//					String getName = control.addBookToBasket(getNameFromISBN);
					int totalElement = listModel.getSize ( );
					listModel.addElement((totalElement+1)+":"+getNameFromISBN); 
					listBasket.setModel(listModel);
					getNameFromISBN = "";
				
				
				}
			}
		});
		addToBasket.setBounds(10, 21, 138, 23);
		panelBuy.add(addToBasket);
		
		JButton clearBasket = new JButton("เคลียร์ตะกร้า");
		clearBasket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
				listBasket.setModel(listModel);
				
				
			}
		});
		clearBasket.setBounds(10, 55, 138, 23);
		panelBuy.add(clearBasket);
		
		JButton buyBook = new JButton("ซื้อสินค้า");
		buyBook.setBounds(10, 264, 138, 23);
		buyBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				
			}
		});
		panelBuy.add(buyBook);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 89, 138, 164);
		panelBuy.add(scrollPane_1);
		
		listBasket = new JList();
		scrollPane_1.setViewportView(listBasket);
		
		listModel = new DefaultListModel( ); 
		
	}
}
