/**
 * Palida Sunthonkiti 5610404487
 */

package ku.cs.shop;

public class Textbook extends Book {
	private String bookEdition;
	private String bookLanguage;
	
	public Textbook(String bookName, String bookAuthor, String bookPrice,
			String bookISBN ,String bookEdition,String bookType) {
		super(bookName, bookAuthor, bookPrice, bookISBN);
		// TODO Auto-generated constructor stub
		this.bookEdition = bookEdition;
		this.bookLanguage = bookType;
	}
	public String getbookEdition(){
		return bookEdition;
	}
	public String getbookLanguage(){
		return bookLanguage;
	}
	
	public String toString(){
		return super.toString()+
				"\nEdition: "+bookEdition+
				"\nLanguage: "+bookLanguage
				;
	}
	
}
