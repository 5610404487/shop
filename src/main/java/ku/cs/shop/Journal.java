/**
 * Palida Sunthonkiti 5610404487
 */

package ku.cs.shop;

public class Journal extends Book{

	private String bookPublishDate;
	private String bookVolumeNumber;

	public Journal(String bookName, String bookAuthor, String bookPrice,
			String bookISBN,String bookPublishDate, String bookVolumeNumber) {
		super(bookName, bookAuthor, bookPrice, bookISBN);
		// TODO Auto-generated constructor stub
		this.bookPublishDate = bookPublishDate;
		this.bookVolumeNumber = bookVolumeNumber;
	}
	public String getbookPublishDate(){
		return bookPublishDate;
	}
	public String getbookVolumeNumber(){
		return bookVolumeNumber;
	}
	public String toString(){
		return super.toString()+
				"\nPublish Date: "+bookPublishDate+
				"\nVolume Number: "+bookVolumeNumber
				;
	}
}
